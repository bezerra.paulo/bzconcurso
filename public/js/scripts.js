var temasSemelhantes = new Bloodhound({
    datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    remote: {
        url: '/temas?key=%QUERY',
        wildcard: '%QUERY'
    }
});

$('#busca .campo-buscar').typeahead(null,
    {
        name: 'temas-semelhantes',
        display: 'tema',
        source: temasSemelhantes
    });

function carregarNoticias(button, seletor, query) {
    if (!query) query = {}
    if (typeof query === 'string') query = JSON.parse(query)
    query.page = page;
    page++;
    $(button).hide();
    $.get(url, query, function(data){
        if (data) {
            $(seletor).append(data);
            $(button).show();
        }else{
            $(seletor).append('<p class="center text-muted">Fim da lista de notícias.</p>');
        }
    })
}