$('#campoAtributo').hide()
$('#campoFormato').hide()

var changeLeitor = function () {
    var leitor = $("#selectLeitor").val()
    if (leitor === 'atributo' || leitor === 'atributos' || leitor === 'imagem') {
        $('#campoAtributo').show()
        return
    }
    $('#campoAtributo').hide()
}

var changeCampo = function () {
    var leitor = $("#selectCampo").val()
    if (leitor === 'publicacao') {
        $('#campoFormato').show()
        return
    }
    $('#campoFormato').hide()
}