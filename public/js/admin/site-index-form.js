var templateRow = `
                        <tr>
                            <td>[campo]</td>
                            <td>[valor]</td>
                            <td>
                                <button type="button" class="btn btn-sm btn-danger" onclick="removerCampo([index])">
                                    <i class="fa fa-times"></i>
                                </button>
                            </td>
                        </tr>
                    `
var updateTableCampos = function () {
    $('#tableCamposPadrao tbody').html("")
    var camposPadrao = JSON.parse($('#camposPadrao').val())
    if (camposPadrao.length < 1)
        return
    $('#tableCamposPadrao').removeClass('sr-only')
    $.each(camposPadrao, function (index, campo) {
        var row = templateRow
            .replace('[campo]', campo.campo)
            .replace('[valor]', campo.valor)
            .replace('[index]', index)

        $('#tableCamposPadrao tbody').append(row)
    })
}

var addCampo = function () {
    if ($('#selectCampo').val().length < 1) {
        swal("O campo deve ser informado.")
        $('#selectCampo').focus()
        return;
    }
    if ($('#inputValor').val().length < 1) {
        swal("O valor deve ser informado.")
        $('#inputValor').focus()
        return;
    }

    var campo = {
        campo: $('#selectCampo').val(),
        valor: $('#inputValor').val(),
    }
    $('#selectCampo').val('')
    $('#inputValor').val('')
    var camposPadrao = []
    if ($('#camposPadrao').val().length > 0)
        camposPadrao = JSON.parse($('#camposPadrao').val())

    var test = camposPadrao.find(function (c) {
        return c.campo === campo.campo
    })

    if (test) {
        swal("O campo informado foi selecionado anteriormente.")
        $('#selectCampo').focus()
        return;
    }

    camposPadrao.push(campo)
    $('#camposPadrao').val(JSON.stringify(camposPadrao))
    updateTableCampos()
}

var removerCampo = function (index) {
    if ($('#camposPadrao').val().length < 1)
        return
    var camposPadrao = JSON.parse($('#camposPadrao').val())
    camposPadrao.splice(index, 1);
    $('#camposPadrao').val(JSON.stringify(camposPadrao))
    updateTableCampos()
}

updateTableCampos()