function excluir(url, redirect) {
    swal({
        title: "Você tem certeza?",
        text: "Caso confirme, a operação de exclusão não pode ser desfeita.",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    }).then((willDelete) => {
        if (willDelete) {
            if (redirect && redirect.length > 0) {
                url += url.indexOf('?') > -1 ? '&' : '?';
                url += "redirect=" + redirect
            }
            window.location = url
        }
    })
}

function excluirTodos(selector, url, redirect) {
    var ids = $(selector).map(function () {
        return $(this).val()
    }).toArray()

    if (ids.length < 1) {
        swal("Pelo menos uma notícia deve ser selecionada!");
        return;
    }

    return excluir(url + '?ids=' + ids.join(), redirect)
}


function toggleCheckbox(selector) {
    $(selector).attr('checked', !$(selector).attr('checked'))
    if ($(selector).attr('checked')) {
        $(selector).parent().parent().addClass('table-info');
    } else {
        $(selector).parent().parent().removeClass('table-info');
    }
}

function toggleAllCheckbox(selector, value) {
    $(selector).attr('checked', value);
    if (value) {
        $(selector).parent().parent().addClass('table-info');
    } else {
        $(selector).parent().parent().removeClass('table-info');
    }
}

function resetPagination() {
    document.formBusca.skip.value = 0
    document.formBusca.submit()
}