var _ = require('underscore')

class PublisherBoot {

    constructor(noticias) {
        this._noticias = noticias
    }

    static async run(noticias, callback) {
        try {
            var publisher = new PublisherBoot(noticias)
            publisher.validarNoticias()
            callback(publisher._noticias)
        } catch (err) {
            console.log(err)
        }
    }

    validarNoticias() {
        this._noticias.forEach(noticia => this.validar(noticia))
    }

    validar(noticia) {
        try {
            if (!noticia.paragrafos) {
                noticia.publisherLog.push('Não foi possivel encontrar os "parágrafos" ')
                noticia.status = 'trashed'
            } else if (!noticia.titulo) {
                noticia.publisherLog.push('Não foi possivel encontrar o "título"')
                noticia.status = 'trashed'
            } else if (!noticia.regiao) {
                noticia.publisherLog.push('Não foi possivel encontrar a "região"')
                noticia.status = 'trashed'
            } else if (noticia.regiao && noticia.regiao.toUpperCase() !== 'NACIONAL' && !noticia.estado) {
                noticia.publisherLog.push('Não foi possivel encontrar a "estado"')
                noticia.status = 'trashed'
            } else if (!noticia.publicacao) {
                noticia.publisherLog.push('Não foi possivel encontrar a "publicacao"')
                noticia.status = 'trashed'
            } else if (!noticia.subtitulo) {
                noticia.publisherLog.push('Não foi possivel encontrar o "subtitulo"')
                noticia.status = 'trashed'
            }
            if (noticia.publisherLog.length === 0) {
                noticia.status = 'published'
            }
        } catch (e) {
            console.log(e)
        }
    }
}

exports = module.exports = PublisherBoot