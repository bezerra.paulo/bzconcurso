var axios = require('axios');
var mongoose = require("mongoose")
var NoticiasScheme = require('../models/NoticiasScheme')
var Reader = require('./utils/Reader')
var _ = require('underscore')
var Noticia = mongoose.model('Noticia', NoticiasScheme)

class IndexerBoot {
    constructor(site, dry) {
        this._site = site
        this._dry = dry
        this._grupo = null
        this._seletores = null
        this._url = null
        this._noticias = []
        this._estados = ['AM', 'RR', 'AP', 'PA', 'TO', 'RO', 'AC', 'MA', 'PI', 'CE', 'RN', 'PE', 'PB', 'SE', 'AL', 'BA',
            'MT', 'MS', 'GO', 'DF', 'SP', 'RJ', 'ES', 'MG', 'PR', 'RS', 'SC']
    }


    static async run(site, callback, dry) {
        try {
            var indexer = new IndexerBoot(site, dry)
            indexer.validateSite()
            indexer.prepareGroup()
            indexer.prepareSeletores()
            indexer.prepareUrl()
            indexer.indexPage(indexer._site.urlsIndex, () => {
                callback(indexer._noticias)
            })
        } catch (err) {
            return err
        }
    }

    validateSite() {
        if (!this._site.urlsIndex || this._site.urlsIndex.length < 1) {
            throw new Error('O site não possui páginas de index cadastradas.')
        }

        if (!this._site.seletores || this._site.seletores.length < 1) {
            throw new Error('O site não possui seletores cadastrados.')
        }
    }

    prepareGroup() {
        this._grupo = this._site.seletores.find(seletor => seletor.campo === 'grupo')
    }

    prepareSeletores() {
        if (!this._grupo) return

        this._seletores = this._site.seletores.filter(seletor => {
            if (seletor.etapa === 'index' && seletor.campo !== 'grupo') {
                return seletor
            }
        })
    }

    prepareUrl() {
        this._url = this._site.seletores.find(seletor => seletor.campo === 'url')
        if (!this._url || this._url.length < 1) {
            throw new Error('O site não possui o seletor de url cadastrado.')
        }
    }

    indexPage(indexes, callback) {
        try {
            var done = _.after(this._site.urlsIndex.length, callback)
            indexes.forEach(async index => {
                if (index.interarEstados) {
                    var doneEstados = _.after(this._estados.length, done)
                    this._estados.forEach(async estado => {
                        var url = index.url.replace('[estado]', estado.toLowerCase())
                        this.extract(url, doneEstados, estado, index.camposPadrao)
                    })
                }  else {
                    this.extract(index.url, done, null, index.camposPadrao)
                }

            })
        } catch (err) {
            console.log(err)
        }
    }

    async extract(url, callback, estado, camposPadrao) {
        var response = await axios.get(url)
        if (this._grupo) {
            this.extractGrupos(response.data, callback, estado, camposPadrao)
        } else {
            this.extractUrls(response.data, callback, estado, camposPadrao)
        }
    }

    extractGrupos(html, callback, estado, camposPadrao) {
        var reader = new Reader(html)
        reader.read(this._grupo, (elements) => {
            var done = _.after(elements.length, callback)
            elements.forEach(element => {
                this.fillGrupo(element, done, estado, camposPadrao)
            })
        })
    }

    fillGrupo(element, callback, estado, camposPadrao) {
        var readerGrupo = new Reader(element)
        var done = _.after(this._seletores.length, callback)
        this._noticias.push(new Noticia)
        var index = this._noticias.length - 1
        if (estado) {
            this._noticias[index].estado = estado
        }
        this._seletores.forEach(seletor => {
            readerGrupo.read(seletor, (data) => {
                this._noticias[index][seletor.campo] = data
                this._noticias[index].site = this._site._id
                this.fillCamposPadrao(this._noticias[index], camposPadrao)
                done()
            })
        })
    }

    extractUrls(html, callback, estado, camposPadrao) {
        var reader = new Reader(html)
        reader.read(this._url, (elements) => {
            if (!elements) {
                callback([])
                return
            }
            var done = _.after(elements.length, callback)
            elements.forEach(element => {
                this._noticias.push(new Noticia)
                var index = this._noticias.length - 1
                if (estado) {
                    this._noticias[index].estado = estado
                }
                this._noticias[index][this._url.campo] = element
                this._noticias[index].site = this._dry ? this._site : this._site._id
                this.fillCamposPadrao(this._noticias[index], camposPadrao)
                done()
            })
        })
    }

    fillCamposPadrao(noticia, campos) {
        if (!campos || campos.length < 1) return
        campos.forEach(campo => noticia[campo.campo] = campo.valor)
    }
}

exports = module.exports = IndexerBoot