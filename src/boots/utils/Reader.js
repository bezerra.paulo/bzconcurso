var cheerio = require('cheerio');
var base64 = require('node-base64-image');
var _ = require('underscore')

class Reader {
    constructor(html) {
        this.$ = cheerio.load(html)
    }

    texto(seletor, callback) {
        this.$(seletor).filter((index, element) => {
            var data = this.$(element)
            callback(data.text())
        })
    }

    textos(seletor, callback) {
        this.$(seletor).each((index, element) => {
            var data = this.$(element)
            callback(data.text())
        })
    }

    atributo(seletor, atributo, callback) {
        var data = this.$(seletor).attr(atributo)
        callback(data)
    }

    atributos(seletor, atributo, callback) {
        var array = []
        var done = _.after(this.$(seletor).length, () => {
            callback(array)
        })
        this.$(seletor).each((index, element) => {
            var data = this.$(element)
            array.push(data.attr(atributo))
            done()
        })
    }

    elemento(seletor, callback) {
        this.$(seletor).filter((index, element) => {
            var data = this.$(element)
            callback(data.html())
        })
    }

    elementos(seletor, callback) {
        var array = []
        var done = _.after(this.$(seletor).length, () => {
            callback(array)
        })
        this.$(seletor).each((index, element) => {
            var data = this.$(element)
            array.push(data.html())
            done()
        })
    }

    imagem(seletor, atributo, callback) {
        this.$(seletor).filter(async (index, element) => {
            try {
                var url = this.$(element).attr(atributo)
                base64.encode(url, {string: true}, (err, image) => {
                    if (err) callback(null)
                    callback(image)
                })
            } catch (e) {
                console.log(e)
                callback(null)
            }
        })
    }

    read(seletor, callback) {
        if (this.$(seletor.seletor).length === 0) {
            callback(null)
        }
        try {
            if (['atributo', 'atributos', 'imagem'].indexOf(seletor.leitor) > -1) {
                this[seletor.leitor](seletor.seletor, seletor.atributo, callback)
            } else {
                this[seletor.leitor](seletor.seletor, callback)
            }
        } catch (err) {
            console.log(err)
            callback(null)
        }
    }
}

exports = module.exports = Reader;