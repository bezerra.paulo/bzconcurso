var axios = require('axios');
var mongoose = require("mongoose")
var NoticiasScheme = require('../models/NoticiasScheme')
var Reader = require('./utils/Reader')
var _ = require('underscore')
var Noticia = mongoose.model('Noticia', NoticiasScheme)
var moment = require("moment");

class ReaderBoot {

    constructor(noticias) {
        this._noticias = noticias
    }

    static async run(noticias, callback) {
        try {
            var reader = new ReaderBoot(noticias)
            reader.readNews(() => {
                callback(reader._noticias)
            })
        } catch (err) {
            return err
        }
    }

    prepareSeletores(site) {
        return site.seletores.filter(seletor => {
            if (seletor.etapa === 'read') {
                return seletor
            }
        })
    }

    async readNews(callback) {
        var done = _.after(this._noticias.length, callback)
        this._noticias.forEach(async noticia => {
            try {
                this.readANew(noticia, done)
            } catch (e) {
                console.log(e)
                done()
            }
        })

    }


    async readANew(noticia, callback) {
        try {
            var response = await axios.get(noticia.url)
            var reader = new Reader(response.data)
            var seletores = this.prepareSeletores(noticia.site)
            var done = _.after(seletores.length, () => {
                if (noticia['postRead'] && typeof noticia['postRead'] === 'function') {
                    noticia['postRead']()
                }
                callback()
            })
            noticia.status = 'read'
            seletores.forEach(seletor => {
                try {
                    reader.read(seletor, (data) => {
                        if (seletor.campo === 'publicacao' && data) {
                            data = moment(data.trim(), seletor.formato, true).isValid()
                                ? moment(data.trim(), seletor.formato, true).toDate() : new Date()
                        }
                        noticia[seletor.campo] = data
                        done()
                    })
                } catch (e) {
                    console.log([e, seletor, noticia])
                    done()
                }
            })
        } catch (err) {
            noticia.status = 'wrong'
            console.log(err)
        }
    }
}

exports = module.exports = ReaderBoot