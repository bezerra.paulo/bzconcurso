var SiteScheme = require('../../models/SiteScheme')
var mongoose = require('mongoose')
var ObjectId = mongoose.Types.ObjectId;

var model = mongoose.model('Site', SiteScheme)

class SiteSeletorController {

    static async new(req, res) {
        var site = await model.findOne({'_id': req.params.id}).exec()
        var seletor = req.query.seletor ? JSON.parse(decodeURIComponent(req.query.seletor)) : {}
        res.render('admin/sites/seletor/new.pug', {
            seletor: seletor,
            site: site,
            page: 'sites',
            campos: campos,
            etapas: etapas,
            leitores: leitores,
            info: req.flash('info'),
            success: req.flash('success'),
            error: req.flash('error'),
        })
    }

    static async create(req, res) {
        var seletor = {
            campo: req.body.campo,
            etapa: req.body.etapa,
            leitor: req.body.leitor,
            seletor: req.body.seletor,
            atributo: req.body.atributo,
            formato: req.body.formato
        }
        console.log(seletor)
        var site = await model.findOne({'_id': req.params.id}).exec()
        site.seletores.push(seletor)
        site.save((err) => {
            if (err) {
                console.log(err)
                req.flash('error', 'Erro ao cadastrar novo seletor.')
                return res.redirect('/admin/sites/seletor/new?site=' + encodeURIComponent(JSON.stringify(req.body)))
            }
            req.flash('success', 'Nova lista de notícias cadastrada com sucesso!')
            return res.redirect('/admin/sites/view/' + site._id + '?tab=seletor');
        })
    }

    static async edit(req, res) {
        var site = await model.findOne({'_id': req.params.id}).exec()
        var seletor = req.query.site ? JSON.parse(decodeURIComponent(req.query.seletor)) : null
        if (!seletor) {
            seletor = site.seletores.id(req.params.seletorId);
        }
        res.render('admin/sites/seletor/edit.pug', {
            seletor: seletor,
            site: site,
            page: 'sites',
            campos: campos,
            etapas: etapas,
            leitores: leitores,
            info: req.flash('info'),
            success: req.flash('success'),
            error: req.flash('error'),
        })
    }

    static async update(req, res) {
        var site = await model.findOne({'_id': req.params.id}).exec()
        var seletor = site.seletores.id(req.params.seletorId);
        seletor.campo = req.body.campo
        seletor.etapa = req.body.etapa
        seletor.leitor = req.body.leitor
        seletor.seletor = req.body.seletor
        seletor.atributo = req.body.atributo
        seletor.formato = req.body.formato
        console.log(seletor)
        site.save((err) => {
            if (err) {
                req.flash('error', 'Erro ao atualizar seletore.')
                return res.redirect('/admin/sites/' + site._id + '/seletor/edit/' + seletor._id
                    + '?seletor=' + encodeURIComponent(JSON.stringify(req.body)))
            }
            req.flash('success', 'Seletor atualizada com sucesso!')
            return res.redirect('/admin/sites/view/' + site._id + '?tab=seletor');
        })
    }

    static async remove(req, res) {
        try {
            console.log(req.params)
            await model.findByIdAndUpdate(req.params.id, {
                $pull: {
                    seletores: {_id: req.params.seletorId}
                }
            }).exec()
            req.flash('success', 'Seletor removido com sucesso')
        } catch (err) {
            console.log(err)
            req.flash('error', 'Site não encontrado')
        }
        return res.redirect('/admin/sites/view/' + req.params.id + '?tab=seletor');
    }
}


var campos = [
    {
        value: "titulo",
        label: "Título",
    },
    {
        value: "subtitulo",
        label: "Subtítulo",
    },
    {
        value: "estado",
        label: "Estado",
    },
    {
        value: "url",
        label: "URL",
    },
    {
        value: "imagem",
        label: "Imagem",
    },
    {
        value: "paragrafos",
        label: "Paragrafos",
    },
    {
        value: "publicacao",
        label: "Data da Publicação",
    },
    {
        value: "grupo",
        label: "Grupo",
    }
]

var etapas = [
    {
        value: 'index',
        label: 'Indexação'
    },
    {
        value: 'read',
        label: 'Leitura'
    }
]

var leitores = [
    {
        value: 'texto',
        label: 'Texto'
    },
    {
        value: 'textos',
        label: 'Textos'
    },
    {
        value: 'atributo',
        label: 'Atributo'
    },
    {
        value: 'atributos',
        label: 'Atributos'
    },
    {
        value: 'elemento',
        label: 'Elemento'
    },
    {
        value: 'elementos',
        label: 'Elementos'
    },
    {
        value: 'imagem',
        label: 'Imagem'
    }
]

exports = module.exports = SiteSeletorController;