var SiteScheme = require('../../models/SiteScheme')
var mongoose = require('mongoose')
var ObjectId = mongoose.Types.ObjectId;

var model = mongoose.model('Site', SiteScheme)

class SiteIndexController {

    static async new(req, res) {
        var site = await model.findOne({'_id': req.params.id}).exec()
        var index = req.query.index ? JSON.parse(decodeURIComponent(req.query.index)) : {
            camposPadrao: []
        }
        res.render('admin/sites/index/new.pug', {
            index: index,
            site: site,
            page: 'sites',
            campos: campos,
            info: req.flash('info'),
            success: req.flash('success'),
            error: req.flash('error'),
        })
    }

    static async create(req, res) {
        var index = {
            url: req.body.url,
            interarEstados: req.body.interarEstados,
            camposPadrao: JSON.parse(req.body.camposPadrao)
        }
        console.log(index)
        var site = await model.findOne({'_id': req.params.id}).exec()
        site.urlsIndex.push(index)
        site.save((err) => {
            if (err) {
                console.log(err)
                req.flash('error', 'Erro ao cadastrar nova lista de notícias.')
                return res.redirect('/admin/sites/index/new?site=' + encodeURIComponent(JSON.stringify(req.body)))
            }
            req.flash('success', 'Nova lista de notícias cadastrada com sucesso!')
            return res.redirect('/admin/sites/view/' + site._id + '?tab=index');
        })
    }

    static async edit(req, res) {
        var site = await model.findOne({'_id': req.params.id}).exec()
        var index = req.query.site ? JSON.parse(decodeURIComponent(req.query.index)) : null
        if (!index) {
            index = site.urlsIndex.id(req.params.indexId);
        }
        res.render('admin/sites/index/edit.pug', {
            index: index,
            site: site,
            page: 'sites',
            campos: campos,
            info: req.flash('info'),
            success: req.flash('success'),
            error: req.flash('error'),
        })
    }

    static async update(req, res) {
        var site = await model.findOne({'_id': req.params.id}).exec()
        var index = site.urlsIndex.id(req.params.indexId);
        index.url = req.body.url
        index.interarEstados = req.body.interarEstados
        index.camposPadrao = JSON.parse(req.body.camposPadrao)
        console.log(index)
        site.save((err) => {
            if (err) {
                req.flash('error', 'Erro ao atualizar lista de notícias.')
                return res.redirect('/admin/sites/' + site._id + '/index/edit/' + index._id
                    + '?index=' + encodeURIComponent(JSON.stringify(req.body)))
            }
            req.flash('success', 'Lista de notícias atualizada com sucesso!')
            return res.redirect('/admin/sites/view/' + site._id + '?tab=index');
        })
    }

    static async remove(req, res) {
        try {
            console.log(req.params)
            await model.findByIdAndUpdate(req.params.id, {
                $pull: {
                    urlsIndex: {_id: req.params.indexId}
                }
            }).exec()
            req.flash('success', 'Index removido com sucesso')
        } catch (err) {
            console.log(err)
            req.flash('error', 'Site não encontrado')
        }
        return res.redirect('/admin/sites/view/' + req.params.id + '?tab=index');
    }
}


var campos = [
    {
        value: "titulo",
        label: "Título",
    },
    {
        value: "subtitulo",
        label: "Subtítulo",
    },
    {
        value: "regiao",
        label: "Região",
    },
    {
        value: "estado",
        label: "Estado",
    },
    {
        value: "url",
        label: "URL",
    },
    {
        value: "imagem",
        label: "Imagem",
    },
    {
        value: "paragrafos",
        label: "Paragrafos",
    }
]

exports = module.exports = SiteIndexController;