var NoticiaScheme = require('../../models/NoticiasScheme')
var SiteScheme = require('../../models/SiteScheme')
var mongoose = require('mongoose')
var moment = require('moment')
var ReaderBoot = require('../../boots/ReaderBoot')
var PublisherBoot = require('../../boots/PublisherBoot')

var estados = {
    'NACIONAL': [],
    'NORTE': ['AM', 'RR', 'AP', 'PA', 'TO', 'RO', 'AC'],
    'NORDESTE': ['MA', 'PI', 'CE', 'RN', 'PE', 'PB', 'SE', 'AL', 'BA'],
    'CENTRO-OESTE': ['MT', 'MS', 'GO', 'DF'],
    'SUDESTE': ['SP', 'RJ', 'ES', 'MG'],
    'SUL': ['PR', 'RS', 'SC']
}

var statuses = ['indexed', 'read', 'wrong', 'published', 'trashed']
var model = mongoose.model('Noticia', NoticiaScheme)
var modelSites = mongoose.model('Site', SiteScheme)

class NoticiaController {

    static async index(req, res) {
        var sites = await modelSites.find().exec()

        var renderizarBusca = (parametros, paginacao, data) => {
            res.render('admin/noticias/index.pug', {
                estados: estados,
                statuses: statuses,
                parametros: parametros,
                paginacao: paginacao,
                noticias: data,
                sites: sites,
                urlRedirect: encodeURIComponent(req.originalUrl),
                page: 'noticias',
                info: req.flash('info'),
                success: req.flash('success'),
                error: req.flash('error')
            })
        }

        try {
            var filtros = {}
            var paginacao = {
                limit: 25,
                skip: 0,
                sort: '-createdAt'
            }
            var parametros = req.query

            if (parametros.regiao) {
                filtros['regiao'] = parametros.regiao
            }

            if (parametros.estado) {
                filtros['estado'] = parametros.estado
            }

            if (parametros.status) {
                filtros['status'] = parametros.status
            }

            if (parametros.site && parametros.site.length > 0) {
                filtros['site'] = parametros.site
            }

            parametros.periodoInicio = (parametros.periodoInicio) ?
                moment(parametros.periodoInicio, 'DD/MM/YYYY') : moment().subtract(7, 'days').toDate();
            parametros.periodoFim = (parametros.periodoFim) ?
                moment(parametros.periodoFim, 'DD/MM/YYYY') : moment().add(1, 'days').toDate();
            filtros['publicacao'] = {'$gte': parametros.periodoInicio, '$lte': parametros.periodoFim}


            if (parametros.limit && parametros.limit.length > 0) {
                paginacao.limit = parseInt(req.query.limit)
            }

            if (parametros.skip && parametros.skip.length > 0) {
                paginacao.skip = parseInt(req.query.skip)
            }

            if (parametros.sort && parametros.sort.length > 0) {
                paginacao.sort = req.query.sort
            }

            if (req.query.key) {
                model.search(req.query.key, {}, {
                    limit: paginacao.limit,
                    skip: paginacao.skip,
                    sort: paginacao.sort,
                    conditions: filtros,
                    populate: [{path: 'site'}]
                }, function (err, data) {
                    if (err) {
                        console.log(err)
                        req.flash('error', "Erro ao consultar notícias com os parâmetros fornecidos.")
                        return res.redirect('/admin')
                    }
                    paginacao.count = data.totalCount

                    return renderizarBusca(parametros, paginacao, data.results)

                });
            } else {

                var data = await model.find(filtros)
                    .limit(paginacao.limit)
                    .skip(paginacao.skip)
                    .sort(paginacao.sort)
                    .populate('site')
                    .exec()

                paginacao.count = await model.find(filtros)
                    .sort(paginacao.sort)
                    .count()
                    .exec()

                return renderizarBusca(parametros, paginacao, data)
            }
        } catch (e) {
            console.log(e)
            req.flash('error', "Erro ao consultar notícias com os parâmetros fornecidos.")
            res.redirect('/admin')
        }
    }

    static async view(req, res) {
        var sites = await modelSites.find().exec()
        var noticia = await model.findOne({'_id': req.params.id}).populate('site').exec()
        if (!noticia) {
            req.flash("info", "Notícia não encontrada")
            return redirectToIndex(req, res)
        }

        res.render('admin/noticias/view.pug', {
            noticia: noticia,
            page: 'noticias',
            sites: sites,
            viewUrlRedirect: encodeURIComponent(req.originalUrl),
            urlRedirect: req.query.redirect,
            info: req.flash('info'),
            success: req.flash('success'),
            error: req.flash('error')
        })
    }

    static async remove(req, res) {
        try {
            await model.findByIdAndRemove(req.params.id).exec()
            req.flash('success', 'Noticia removida com sucesso')
        } catch (err) {
            req.flash('error', 'Noticia não encontrada')
        }
        return redirectToIndex(req, res);
    }

    static async removeAll(req, res) {
        try {
            var ids = req.query.ids.split(',')
            await model.deleteMany({'_id': {$in: ids}}).exec()
            req.flash('success', 'Notícias removidas com sucesso')
        } catch (err) {
            console.log(err)
            req.flash('error', 'Notícias não encontradas')
        }
        return redirectToIndex(req, res);
    }

    static async indexarBusca(req, res) {
        model.setKeywords(function (err) {
            if (err)
                req.flash({'error': 'Deu ruim!'})
            else
                req.flash({'success': 'Ta no jeito chefe!'})
            return redirectToIndex(req, res);
        });
    }

    static async indexarNoticias(req, res) {
        ScrapeController.run()
        req.flash({'info': 'Indexando notícias...'})
        return redirectToIndex(req, res);
    }

    static async update(req, res) {
        try {
            var noticia = await model.findById(req.params.id).populate('site').exec()
            ReaderBoot.run([noticia], noticias => {
                PublisherBoot.run(noticias, noticias => {
                    noticias[0].save()
                    if (noticias[0].status === 'published') {
                        req.flash('success', 'Notícia atualizada com sucesso')
                    } else {
                        req.flash('info', 'Notícia atualizada mas não pode ser publicada: ' + noticias[0].publisherLog)
                    }
                    return res.redirect(decodeURIComponent(req.query.redirect))
                })
            })
        } catch (err) {
            req.flash('error', 'Não foi possivel atualizar a notícia')
            return res.redirect(decodeURIComponent(req.query.redirect))
        }
    }

}

var redirectToIndex = function (req, res) {
    var urlRedirect = '/admin/noticias';
    if (req.query.redirect && req.query.redirect.length > 0)
        urlRedirect = decodeURIComponent(req.query.redirect)
    console.log(urlRedirect);
    return res.redirect(urlRedirect)
}

exports = module.exports = NoticiaController;