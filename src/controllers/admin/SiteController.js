var SiteScheme = require('../../models/SiteScheme')
var mongoose = require('mongoose')
var ObjectId = mongoose.Types.ObjectId;
var IndexerBoot = require('../../boots/IndexerBoot')
var ReaderBoot = require('../../boots/ReaderBoot')
var PublisherBoot = require('../../boots/PublisherBoot')
var _ = require('underscore')

var model = mongoose.model('Site', SiteScheme)

class SiteController {

    static async index(req, res) {
        try {
            var filtros = {}
            var paginacao = {
                limit: 25,
                skip: 0,
                sort: '-createdAt'
            }
            var parametros = req.query

            if (parametros.regiao) {
                filtros['regiao'] = parametros.regiao
            }

            if (parametros.limit && parametros.limit.length > 0) {
                paginacao.limit = parseInt(req.query.limit)
            }

            if (parametros.skip && parametros.skip.length > 0) {
                paginacao.skip = parseInt(req.query.skip)
            }

            if (parametros.sort && parametros.sort.length > 0) {
                paginacao.sort = req.query.sort
            }

            var sites = await model.find(filtros)
                .limit(paginacao.limit)
                .skip(paginacao.skip)
                .sort(paginacao.sort)
                .exec()

            paginacao.count = await model.find(filtros)
                .sort(paginacao.sort)
                .count()
                .exec()

            res.render('admin/sites/index.pug', {
                parametros: parametros,
                paginacao: paginacao,
                sites: sites,
                page: 'sites',
                info: req.flash('info'),
                success: req.flash('success'),
                error: req.flash('error')
            })
        } catch (e) {
            console.log(e)
            req.flash('error', "Erro ao consultar sites com os parâmetros fornecidos.")
            res.redirect('/admin')
        }
    }

    static async view(req, res) {
        var site = await model.findOne({'_id': req.params.id}).exec()
        var noticias = {}
        if (req.query.tab === 'test' && req.query.start) {
            try {
                IndexerBoot.run(site, noticias => {
                    console.log(noticias.length + ' notícias indexadas')
                    noticias = noticias.splice(0, 20);

                    ReaderBoot.run(noticias, noticias => {
                        console.log(noticias.length + ' notícias lidas')

                        PublisherBoot.run(noticias, noticias => {
                            console.log(noticias.length + ' notícias validadas')
                            res.render('admin/sites/view.pug', {
                                site: site,
                                page: 'sites',
                                noticias: noticias,
                                tab: req.query.tab,
                                info: req.flash('info'),
                                success: req.flash('success'),
                                error: req.flash('error'),
                            })
                        })
                    })
                }, true)
            } catch (err) {
                console.log(err)
            }
            // noticias.read = await ReaderBoot.dryRun(noticia.index.split(1, 10))
            // noticias.publish = await PublisherBoot.dryRun(noticias.read)
        } else {
            res.render('admin/sites/view.pug', {
                site: site,
                page: 'sites',
                noticias: noticias,
                tab: req.query.tab,
                info: req.flash('info'),
                success: req.flash('success'),
                error: req.flash('error'),
            })
        }
    }

    static async new(req, res) {
        var site = req.query.site ? JSON.parse(decodeURIComponent(req.query.site)) : {}
        res.render('admin/sites/new.pug', {
            site: site,
            page: 'sites',
            info: req.flash('info'),
            success: req.flash('success'),
            error: req.flash('error'),
        })
    }

    static async create(req, res) {
        var site = new model({
            urlRoot: req.body.urlRoot,
            apelido: req.body.apelido,
            titulo: req.body.titulo,
        })
        site.save((err, newSite) => {
            if (err) {
                console.log(err)
                req.flash('error', 'Erro ao cadastrar novo site.')
                return res.redirect('/admin/sites/new/?site=' + encodeURIComponent(JSON.stringify(req.body)))
            }
            req.flash('success', 'Novo site cadastrado com sucesso!')
            return res.redirect('/admin/sites/view/' + newSite._id);
        })
    }

    static async edit(req, res) {
        var site = await model.findOne({'_id': req.params.id}).exec()
        site = req.query.site ? JSON.parse(decodeURIComponent(req.query.site)) : site
        res.render('admin/sites/edit.pug', {
            site: site,
            page: 'sites',
            info: req.flash('info'),
            success: req.flash('success'),
            error: req.flash('error'),
        })
    }

    static async active(req, res) {
        var site = await model.findOne({'_id': req.params.id}).exec()
        site.ativo = !site.ativo
        site.save((err, newSite) => {
            if (err) {
                console.log(err)
                if (site.ativo)
                    req.flash('error', 'Erro ao ativar site.')
                else
                    req.flash('error', 'Erro ao desativar site.')
                return res.redirect('/admin/sites/new/?site=' + encodeURIComponent(JSON.stringify(req.body)))
            }
            if (site.ativo)
                req.flash('success', 'Site ativado com sucesso!')
            else
                req.flash('success', 'Site desativado com sucesso!')
            var backURL = req.header('Referer') || '/admin/sites';
            return res.redirect(backURL);
        })
    }

    static async update(req, res) {
        var site = await model.findOne({'_id': req.params.id}).exec()
        site.urlRoot = req.body.urlRoot
        site.apelido = req.body.apelido
        site.titulo = req.body.titulo
        site.save((err, newSite) => {
            if (err) {
                console.log(err)
                req.flash('error', 'Erro ao atualizar site.')
                return res.redirect('/admin/sites/new/?site=' + encodeURIComponent(JSON.stringify(req.body)))
            }
            req.flash('success', 'Site atualizado com sucesso!')
            return res.redirect('/admin/sites/view/' + newSite._id);
        })
    }

    static async remove(req, res) {
        try {
            await model.findByIdAndRemove(req.params.id).exec()
            req.flash('success', 'Site removido com sucesso')
        } catch (err) {
            req.flash('error', 'Site não encontrado')
        }
        return res.redirect('/admin/sites');
    }
}

exports = module.exports = SiteController;