var Account = require('../../models/AccountScheme')
var passport = require('passport')

class AccountController {

    static async login(req, res) {
        if (res.user) {
            res.redirect('admin/home')
        }
        res.render('admin/login.pug', {messages: req.flash('error')})
    }

    static async logout(req, res) {
        req.logout();
        res.redirect('/admin/login');
    }

    static async home(req, res) {
        res.render('admin/home.pug', {
            user: req.user,
            page: 'home',
            info: req.flash('info'),
            success: req.flash('success'),
            error: req.flash('error')
        })
    }

    static async registerAdmin(req, res) {
        var user = await Account.findOne({'username': 'admin'}).exec()
        if (user) {
            return res.json('Admin já existe')
        }
        Account.register(new Account({username: 'admin', group: 'ADMIN'}), 'mudar123', function (err, account) {
            if (err) {
                console.log(err)
                return res.json('Erro ao criar admin')
            }
            return res.json('Admin criado com sucesso')
        });
    }

    static async index(req, res) {
        var accounts = await Account.find().exec()
        res.render('admin/accounts/index.pug', {
            accounts: accounts,
            page: 'accounts',
            info: req.flash('info'),
            success: req.flash('success'),
            error: req.flash('error')
        })
    }

    static async new(req, res) {
        var account = req.query.account ? JSON.parse(decodeURIComponent(req.query.account)) : {}
        res.render('admin/accounts/new.pug', {
            account: account,
            page: 'accounts',
            error: req.flash('error')
        })
    }

    static async create(req, res) {
        var user = await Account.findOne({'username': req.body.username}).exec()
        if (user) {
            req.flash('error', 'Já existe um usuário com este nome')
            return res.redirect('/admin/accounts/new?account=' + encodeURIComponent(JSON.stringify(req.body)))
        }
        Account.register(
            new Account({
                username: req.body.username,
                group: req.body.group
            }),
            req.body.password,
            function (err, account) {
                if (err) {
                    console.log(err)
                    req.flash('error', 'Erro ao salvar dados do usuário')
                    return res.redirect('/admin/accounts/new?account=' + encodeURIComponent(JSON.stringify(req.body)))
                }
                req.flash('success', 'Usuário cadastrado com sucesso')
                return res.redirect('/admin/accounts')
            }
        );
    }

    static async edit(req, res) {
        var account = await Account.findOne({'_id': req.params.userId}).exec()
        if (!account) {
            req.flash('error', 'Usuário não encontrado')
            return res.redirect('/admin/accounts')
        }
        account = !req.query.account ? account : JSON.parse(decodeURIComponent(req.query.account));
        res.render('admin/accounts/edit.pug', {
            account: account,
            page: 'accounts',
            error: req.flash('error')
        })
    }

    static async update(req, res) {
        var account = await Account.findOne({'_id': req.params.userId}).exec()
        if (!account) {
            req.flash('error', 'Usuário não encontrado')
            return res.redirect('/admin/accounts/edit' + req.params.userId)
        }
        try {
            account.username = req.body.username
            account.group = req.body.group
            await account.save()
            req.flash('success', 'Usuário cadastrado com sucesso')
            return res.redirect('/admin/accounts')
        } catch (err) {
            console.log(err)
            req.flash('error', 'Erro ao salvar dados do usuário')
            return res.redirect('/admin/accounts/edit' + req.params.userId)
        }
    }

    static async remove(req, res) {
        try {
            var account = await Account.findByIdAndRemove(req.params.userId).exec()
            req.flash('success', 'Usuário removido com sucesso')
        } catch (err) {
            req.flash('error', 'Usuário não encontrado')
        }
        return res.redirect('/admin/accounts')
    }

    static async changePassword(req, res) {
        res.render('admin/accounts/change_password.pug', {
            error: req.flash('error')
        })
    }

    static async savePassword(req, res) {
        if (req.body.password !== req.body.password_confirmation) {
            req.flash('error', 'Senha e confirmação de senha são diferentes.')
            return res.redirect('/admin/accounts/change_password/' + req.params.userId)
        }
        var account = await Account.findOne({'_id': req.params.userId}).exec()
        if (!account) {
            req.flash('error', 'Usuário não encontrado')
            return res.redirect('/admin/accounts/change_password/' + req.params.userId)
        }
        try {
            await account.setPassword(req.body.password)
            await account.save()
            req.flash('success', 'Senha alterada com sucesso')
            return res.redirect('/admin/accounts')
        } catch (err) {
            console.log(err)
            req.flash('error', 'Erro ao alterar senha do usuário')
            return res.redirect('/admin/accounts/change_password/' + req.params.userId)
        }
    }
}

exports = module.exports = AccountController