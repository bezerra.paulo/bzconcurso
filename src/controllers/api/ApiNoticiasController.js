var NoticiaScheme = require('../../models/NoticiasScheme')
var moment = require('moment')
var mongoose = require('mongoose')

var responderComJson = (res, data) => {
    return res.json({
        status: 'OK',
        data: data
    })
}

var responderComJsonErro = (res, error) => {
    return res.json({
        status: 'OK',
        error: error
    })
}

var model = mongoose.model('Noticia', NoticiaScheme)

class ApiNoticiasController {

    async index(req, res) {
        try {
            var filtros = {}
            var paginacao = {
                limit: 25,
                skip: 0,
                sort: '-createdAt'
            }
            var parametros = req.query

            filtros['status'] = 'published'

            if (parametros.regiao) {
                filtros['regiao'] = parametros.regiao
            }

            if (parametros.estado) {
                filtros['estado'] = parametros.estado
            }

            parametros.periodoInicio = (parametros.periodoInicio) ?
                moment(parametros.periodoInicio, 'DD-MM-YYYY') : moment().subtract(7, 'days').toDate();
            parametros.periodoFim = (parametros.periodoFim) ?
                moment(parametros.periodoFim, 'DD-MM-YYYY') : moment().add(1, 'days').toDate();
            filtros['publicacao'] = {'$gte': parametros.periodoInicio, '$lte': parametros.periodoFim}


            if (parametros.limit && parametros.limit.length > 0) {
                paginacao.limit = parseInt(req.query.limit)
            }

            if (parametros.skip && parametros.skip.length > 0) {
                paginacao.skip = parseInt(req.query.skip)
            }

            if (parametros.sort && parametros.sort.length > 0) {
                paginacao.sort = req.query.sort
            }

            var retorno = {}

            if (req.query.key) {
                model.search(req.query.key, {}, {
                    limit: paginacao.limit,
                    skip: paginacao.skip,
                    sort: paginacao.sort,
                    conditions: filtros,
                    populate: [{path: 'site'}]
                }, function (err, data) {
                    if (err) {
                        console.log(err)
                        return responderComJsonErro(res, {
                            message: "Erro ao consultar notícias com os parâmetros fornecidos."
                        })
                    }
                    paginacao.count = data.totalCount

                    retorno = {
                        paginacao: paginacao,
                        noticias: data.results,
                    }

                });
            } else {

                var data = await model.find(filtros)
                    .limit(paginacao.limit)
                    .skip(paginacao.skip)
                    .sort(paginacao.sort)
                    .populate('site')
                    .exec()

                paginacao.count = await model.find(filtros)
                    .sort(paginacao.sort)
                    .count()
                    .exec()

                retorno = {
                    paginacao: paginacao,
                    noticias: data,
                }
            }

            retorno.noticias = retorno.noticias.map(function(noticia){
                return noticia.toJson()
            })

            responderComJson(res, retorno)

        } catch (e) {
            console.log(e)
            return responderComJsonErro(res, {
                message: "Erro ao consultar notícias com os parâmetros fornecidos."
            })
        }
    }

    async view(req, res) {
        var noticia = await model.findOne({'slug': req.params.slug}).populate('site').exec()
        if (!noticia) {
            return responderComJsonErro(res, {
                message: "Notícia não encontrada"
            })
        }

        return responderComJson(res, noticia.toJson())
    }

    regioes(req, res) {
        return responderComJson(res, {
            'NACIONAL': [],
            'NORTE': ['AM', 'RR', 'AP', 'PA', 'TO', 'RO', 'AC'],
            'NORDESTE': ['MA', 'PI', 'CE', 'RN', 'PE', 'PB', 'SE', 'AL', 'BA'],
            'CENTRO-OESTE': ['MT', 'MS', 'GO', 'DF'],
            'SUDESTE': ['SP', 'RJ', 'ES', 'MG'],
            'SUL': ['PR', 'RS', 'SC']
        })
    }
}

exports = module.exports = new ApiNoticiasController();