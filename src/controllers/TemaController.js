var TemasScheme = require('../models/TemasScheme')
var mongoose = require('mongoose')

var model = mongoose.model('Tema', TemasScheme)

class TemaController {

    static async buscar(req, res) {
        var data = await model.find({'tema': { $regex: '.*' + req.query.key + '.*' }}).limit(10).exec()
        res.json(data)
    }

}

exports = module.exports = TemaController