var NoticiaScheme = require('../models/NoticiasScheme')
var TemasScheme = require('../models/TemasScheme')
var mongoose = require('mongoose')
var moment = require('moment-timezone')

var estados = {
    'NORTE': ['AM', 'RR', 'AP', 'PA', 'TO', 'RO', 'AC'],
    'NORDESTE': ['MA', 'PI', 'CE', 'RN', 'PE', 'PB', 'SE', 'AL', 'BA'],
    'CENTRO-OESTE': ['MT', 'MS', 'GO', 'DF'],
    'SUDESTE': ['SP', 'RJ', 'ES', 'MG'],
    'SUL': ['PR', 'RS', 'SC']
}

var model = mongoose.model('Noticia', NoticiaScheme)
var modelTema = mongoose.model('Tema', TemasScheme)

var url = require('url');

function fullUrl(req) {
    return url.format({
        protocol: req.protocol,
        host: req.get('host'),
        pathname: req.originalUrl
    });
}

function domainUrl(req) {
    return url.format({
        protocol: req.protocol,
        host: req.get('host'),
    });
}

class NoticiaController {

    static async index(req, res) {
        try {
            var {nacionais, centroOeste, sudeste, sul, norte, nordeste} = await NoticiaController.destaquesPorRegiao();

            var ultimas = await model.find()
                .where({
                    'status': 'published'
                })
                .populate('site')
                .sort('-publicacao')
                .limit(10)
                .exec()

            var destaques = await model.find()
                .where({
                    'status': 'published',
                    'publicacao': {$gte: moment().subtract(7, 'days')}
                })
                .populate('site')
                .sort('-visualizacoes -publicacao')
                .limit(10)
                .exec()

            var tags = await modelTema.find()
                .where({
                    'estatisticas.data': {$gte: moment().subtract(30, 'days')}
                })
                .sort('-estatisticas.qtde')
                .limit(10)


            res.render('index.pug', {
                destaques: destaques,
                ultimas: ultimas,
                nacionais: nacionais,
                centroOeste: centroOeste,
                nordeste: nordeste,
                norte: norte,
                sudeste: sudeste,
                sul: sul,
                url: fullUrl(req),
                baseUrl: domainUrl(req),
                tags: tags
            })
        } catch (err) {
            console.log(err)
            return handleError({'error': err})
        }
    }

    static async get(req, res) {
        try {
            var noticia = await model.findBySlug(req.params.slug).populate('site').exec()
            if (!noticia.visualizacoes) {
                noticia.visualizacoes = 0
            }
            noticia.visualizacoes++
            noticia.save((err, success) => {
                console.log(err)
            })

            var maisNoticias = await model.find()
                .populate('site')
                .sort('-publicacao')
                .where({
                    'regiao': noticia.regiao
                })
                .limit(10)
                .exec()

            res.render('noticia.pug', {
                noticia: noticia,
                url: fullUrl(req),
                maisNoticias: maisNoticias,
                baseUrl: domainUrl(req)
            })
        } catch (err) {
            console.log(err)
            return handleError({'error': err})
        }
    }

    static async regiao(req, res) {
        try {

            var {nacionais, centroOeste, sudeste, sul, norte, nordeste} = await NoticiaController.destaquesPorRegiao();

            var noticias = await model.find()
                .populate('site')
                .sort('-publicacao')
                .where({'regiao': req.params.regiao, 'status': 'published'})
                .limit(10)
                .exec()

            var destaques = await model.find()
                .where({
                    'regiao': req.params.regiao,
                    'status': 'published',
                    'publicacao': {$gte: moment().subtract(7, 'days')}
                })
                .populate('site')
                .sort('-visualizacoes -publicacao')
                .limit(10)
                .exec()

            var tags = await modelTema.find()
                .where({
                    'estatisticas.data': {$gte: moment().subtract(30, 'days')}
                })
                .sort('-estatisticas.qtde')
                .limit(10)

            var listQuery = JSON.stringify({
                regiao: req.params.regiao
            })

            res.render('regiao.pug', {
                regiaoSelecionada: req.params.regiao,
                estados: estados[req.params.regiao],
                noticias: noticias,
                destaques: destaques,
                nacionais: nacionais,
                centroOeste: centroOeste,
                nordeste: nordeste,
                norte: norte,
                sudeste: sudeste,
                sul: sul,
                tags: tags,
                url: fullUrl(req),
                baseUrl: domainUrl(req),
                listQuery: listQuery
            })
        } catch (err) {
            console.log(err)
            return handleError({'error': err})
        }
    }

    static async estado(req, res) {
        try {

            var {nacionais, centroOeste, sudeste, sul, norte, nordeste} = await NoticiaController.destaquesPorRegiao();

            var noticias = await model.find()
                .populate('site')
                .sort('-publicacao')
                .where({'estado': req.params.estado, 'status': 'published'})
                .limit(30)
                .exec()

            var destaques = await model.find()
                .where({
                    'estado': req.params.estado,
                    'status': 'published',
                    'publicacao': {$gte: moment().subtract(7, 'days')}
                })
                .populate('site')
                .sort('-visualizacoes -publicacao')
                .limit(10)
                .exec()

            var tags = await modelTema.find()
                .where({
                    'estatisticas.data': {$gte: moment().subtract(30, 'days')}
                })
                .sort('-estatisticas.qtde')
                .limit(10)

            var listQuery = JSON.stringify({
                estado: req.params.estado
            })

            res.render('regiao.pug', {
                regiaoSelecionada: req.params.regiao,
                estadoSelecionado: req.params.estado,
                estados: estados[req.params.regiao],
                noticias: noticias,
                destaques: destaques,
                nacionais: nacionais,
                centroOeste: centroOeste,
                nordeste: nordeste,
                norte: norte,
                sudeste: sudeste,
                sul: sul,
                tags: tags,
                url: fullUrl(req),
                baseUrl: domainUrl(req),
                listQuery: listQuery
            })
        } catch (err) {
            console.log(err)
            return handleError({'error': err})
        }
    }

    static async buscar(req, res) {
        var tags = await modelTema.find()
            .where({
                'estatisticas.data': {$gte: moment().subtract(30, 'days')}
            })
            .sort('-estatisticas.qtde')
            .limit(30)

        var filtros = {status: 'published'}
        if (req.query.estado)
            filtros['estado'] = req.query.estado
        else if (req.query.regiao)
            filtros['regiao'] = req.query.regiao

        var listQuery = JSON.parse(JSON.stringify(filtros))
        listQuery.key = req.query.key ? req.query.key : req.params.key
0
        if (listQuery.key) listQuery.key = listQuery.key.trim();

        if (!listQuery.key || listQuery.key.length < 1) {
            return res.redirect(req.get('Referer'));
        }

        model.search(listQuery.key, {}, {
            limit: 10,
            conditions: filtros,
            populate: [{path: 'site'}]
        }, function (err, data) {
            new modelTema().registrar(req.query)
            res.render('buscar.pug', {
                regiaoSelecionada: req.query.regiao,
                estadoSelecionado: req.query.estado,
                estados: estados[req.query.regiao],
                noticias: data.results,
                key: listQuery.key,
                tags: tags,
                url: fullUrl(req),
                baseUrl: domainUrl(req),
                listQuery: JSON.stringify(listQuery)
            })
        });
    }

    static async getImagem(req, res) {
        try {
            var noticia = await model.findOne({'_id': req.params.id}).exec()

            var img = new Buffer(noticia.imagem, 'base64');

            res.writeHead(200, {
                'Content-Type': 'image/png',
                'Content-Length': img.length
            });
            res.end(img);
        } catch (err) {
            console.log(err)
            return handleError({'error': err})
        }
    }

    static async getPartialListaNoticias(req, res) {
        try {
            var filtros = {}
            var sort = '-publicacao'
            var limit = 10
            var page = 1
            var hideRegiao = false
            var hideEstado = false

            if (req.query.regiao) {
                filtros.regiao = req.query.regiao
                hideRegiao = true
            }

            if (req.query.estado) {
                filtros.estado = req.query.estado
                hideRegiao = true
                hideEstado = true
            }

            if (req.query.ultimos_dias) {
                filtros.publicacao = {$gte: moment().subtract(req.query.ultimos_dias, 'days')}
            }

            if (req.query.sort) {
                sort = req.query.sort
            }

            if (req.query.limit) {
                limit = req.query.limit
            }

            if (req.query.page) {
                page = req.query.page
            }

            var skip = page > 0 ? (page - 1) * limit : 0

            var query = model.find()
                .where(filtros)
                .populate('site')
                .sort(sort)
                .skip(skip)
                .limit(limit)

            var noticias = await query.exec()
            res.render('partListaNoticias.pug', {
                noticias: noticias,
                hideRegiao: hideRegiao,
                hideEstado: hideEstado
            })
        } catch (err) {
            console.log(err)
            res.render('partListaNoticias.pug', {
                noticias: null,
            })
        }
    }

    static async getPartialBuscaNoticias(req, res) {
        try {
            var filtros = {status: 'published'}
            var limit = 10
            var page = 1
            var hideRegiao = false
            var hideEstado = false

            if (req.query.estado) {
                filtros['estado'] = req.query.estado
                hideRegiao = true
                hideEstado = true
            } else if (req.query.regiao) {
                filtros['regiao'] = req.query.regiao
                hideRegiao = true
            }

            if (req.query.limit) {
                limit = req.query.limit
            }

            if (req.query.page) {
                page = req.query.page
            }

            var skip = page > 0 ? (page - 1) * limit : 0

            model.search(req.query.key, {}, {
                limit: limit,
                skip: skip,
                conditions: filtros,
                populate: [{path: 'site'}]
            }, function (err, data) {
                res.render('partBuscaNoticias.pug', {
                    noticias: data.results,
                    hideRegiao: hideRegiao,
                    hideEstado: hideEstado
                })
            });
        } catch (err) {
            console.log(err)
            res.render('partBuscaNoticias.pug', {
                noticias: null,
            })
        }
    }

    static async destaquesPorRegiao() {
        var query = model.find().populate('site').limit(5).sort('-visualizacoes -publicacao')

        var conditions = {
            'regiao': 'NACIONAL',
            'status': 'published',
            'publicacao': {$gte: moment().subtract(7, 'days')}
        };
        var nacionais = await query.where(conditions).exec()

        conditions.regiao = 'CENTRO-OESTE'
        var centroOeste = await query.where(conditions).exec()

        conditions.regiao = 'SUDESTE'
        var sudeste = await query.where(conditions).exec()

        conditions.regiao = 'SUL'
        var sul = await query.where(conditions).exec()

        conditions.regiao = 'NORTE'
        var norte = await query.where(conditions).exec()

        conditions.regiao = 'NORDESTE'
        var nordeste = await query.where(conditions).exec()
        return {nacionais, centroOeste, sudeste, sul, norte, nordeste};
    }

}

exports = module.exports = NoticiaController