var mongoose = require('mongoose')
var timestamps = require('mongoose-timestamp');
var passportLocalMongoose = require('passport-local-mongoose');

var accountScheme = new mongoose.Schema({
    username: {type: String, require: true},
    password: {type: String, require: true},
    group: {
        type: String,
        enum: ['ADMIN', 'REPORTER'],
        require: true
    }
});

accountScheme.plugin(timestamps);
accountScheme.plugin(passportLocalMongoose);

exports = module.exports = mongoose.model('Account', accountScheme);