var mongoose = require('mongoose')
var moment = require('moment')
var timestamps = require('mongoose-timestamp');

var siteScheme = new mongoose.Schema({
    apelido: {
        type: String,
        require: true,
        unique: true
    },
    titulo: {
        type: String,
        require: true
    },
    urlRoot: {
        type: String,
        unique: true,
        require: true
    },
    ativo: {
      type: Boolean,
      default: false
    },
    urlsIndex: [{
        url: {
            type: String,
            require: true,
        },
        interarEstados: {
            type: Boolean,
            default: false
        },
        camposPadrao: [{
            campo: {
                type: String,
                enum: ['titulo', 'subtitulo', 'regiao', 'estado', 'url', 'imagem', 'paragrafos'],
                require: true
            },
            valor: {
                type: String,
                require: true
            }
        }]
    }],
    seletores: [{
        campo: {
            type: String,
            enum: ['titulo', 'subtitulo', 'regiao', 'estado', 'url', 'imagem', 'paragrafos', 'grupo', 'publicacao'],
            require: true
        },
        etapa: {
            type: String,
            enum: ['index', 'read'],
            require: true
        },
        leitor: {
            type: String,
            enum: ['texto', 'textos', 'atributo', 'atributos', 'elemento', 'elementos', 'imagem'],
            default: 'texto',
            require: true
        },
        seletor: {
            type: String,
            require: true
        },
        atributo: {
            type: String,
            default: null
        },
        formato: {
            type: String,
            default: null
        }
    }],
    lastScraping: {type: Date, default: new Date()},
});

siteScheme.plugin(timestamps);


exports = module.exports = siteScheme;