var mongoose = require('mongoose')
var moment = require('moment')
var timestamps = require('mongoose-timestamp');
var searchPlugin = require('mongoose-search-plugin')
var moment = require('moment')

var temasSchema = new mongoose.Schema({
    tema: {type: String, unique: true},
    estatisticas: [{
        data: Date,
        qtde: {type: Number, default: 0},
    }]
});

temasSchema.plugin(timestamps);

temasSchema.methods.registrar = (params) => {
    var modelTema = mongoose.model('Tema', temasSchema)
    var today = moment().startOf("day");
    modelTema.findOne({'tema': params.key}, (err, tema) => {
        if (err) console.log(err)
        if (tema === null) {
            tema = new modelTema({
                tema: params.key,
                estatisticas: [{
                    data: today,
                    qtde: 1
                }]
            })
        } else {
            var achou = false
            tema.estatisticas.forEach((estatistica, index) => {
                if (today.diff(estatistica.data) === 0) {
                    achou = true;
                    tema.estatisticas[index].qtde++
                }
            })
            if (!achou) {
                tema.estatisticas.push({
                    data: today,
                    qtde: 1
                })
            }
        }
        tema.save((err) => {
            if (err) console.log(err)
        })
    })
}

exports = module.exports = temasSchema;