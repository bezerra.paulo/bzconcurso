var mongoose = require('mongoose')
var moment = require('moment-timezone')
var timestamps = require('mongoose-timestamp');
var searchPlugin = require('mongoose-search-plugin');
var URLSlugs = require('mongoose-url-slugs');

moment.locale('pt-br');
moment.tz.setDefault('America/Campo_Grande');

var noticiasSchema;
noticiasSchema = new mongoose.Schema({
    titulo: String,
    subtitulo: String,
    regiao: {type: String, index: true},
    estado: {type: String, index: true},
    url: {type: String, index: true, unique: true},
    imagem: String,
    paragrafos: [String],
    publicacao: {type: Date, default: new Date(), index: true},
    visualizacoes: {type: Number, default: 0},
    site: {type: mongoose.Schema.Types.ObjectId, ref: 'Site'},
    status: {
        type: String,
        default: 'indexed',
        enum: [
            'indexed', //Criada pelo IndexerBoot falta dados
            'read', //Lida pelo ReaderBoot pronta para ser publicada ou descartada
            'wrong', //Erro ao ser lida pelo ReaderBoot
            'published', //Aprovada pelo PublisherBoot
            'trashed' //Descartada pelo PublisherBoot, provável problema com configuração dos seletores do site
        ]
    },
    publisherLog: [String]
});

noticiasSchema.plugin(timestamps);

noticiasSchema.plugin(searchPlugin, {
    stemmer: 'PorterStemmerPt',
    distance: 'JaroWinklerDistance',
    fields: [{
        name: 'titulo',
        weight: 100,
    }, {
        name: 'subtitulo',
        weight: 50,
    }, {
        name: 'paragrafos',
        weight: 1,
    }]
})

noticiasSchema.plugin(URLSlugs('titulo _id', {maxLength: 120, update: true}));

noticiasSchema.methods.postRead = function () {
    var regioes = {
        'NORTE': ['AM', 'RR', 'AP', 'PA', 'TO', 'RO', 'AC'],
        'NORDESTE': ['MA', 'PI', 'CE', 'RN', 'PE', 'PB', 'SE', 'AL', 'BA'],
        'CENTRO-OESTE': ['MT', 'MS', 'GO', 'DF'],
        'SUDESTE': ['SP', 'RJ', 'ES', 'MG'],
        'SUL': ['PR', 'RS', 'SC']
    }

    this.estado = this.estado ? this.estado.trim() : ''
    Object.keys(regioes).every((regiao) => {
        if (regioes[regiao].indexOf(this.estado) >= 0) {
            this.regiao = regiao
            return false
        }
        return true
    })

    if (!this.regiao) {
        this.estado = null
        this.regiao = 'NACIONAL'
    }

    if (!this.publicacao) {
        this.publicacao = new Date()
    }
}

noticiasSchema.methods.regiaoFormatada = function (hideRegiao, hideEstado) {
    if ((!this.regiao && !this.estado) || (hideRegiao && hideEstado)) return

    var string = null

    if (this.regiao && !hideRegiao) string = this.regiao

    if (this.estado && !hideEstado) {
        if (string)
            string += ' / ' + this.estado
        else
            string = this.estado
    }
    return string
}

noticiasSchema.methods.dataFormatada = function () {
    return new moment(this.publicacao).format('DD/MM/YYYY HH[h]mm')
}

noticiasSchema.methods.visualizacoesFormatada = function () {
    if (this.visualizacoes === 1) {
        return 'Uma visualização'
    }

    if (this.visualizacoes > 1) {
        return this.visualizacoes + ' Visualização'
    }

    return 'Nenhuma visualização'
}

noticiasSchema.methods.regiaoCapitalized = function () {
    if (this.regiao) {
        return this.regiao.toLowerCase().replace(/(^|\s)\S/g, function (a) {
            return a.toUpperCase()
        })
    }
    return ''
}

noticiasSchema.methods.getKeywordsMap = function() {
    return this._keywords.sort((a,b) => b.weight - a.weight).map(key => key.keyword).slice(0, 5)
}

noticiasSchema.methods.dataUTF = function() {
    return moment(this.publicacao).format()
}

noticiasSchema.methods.toJson = function () {
    return {
        titulo: this.titulo,
        subtitulo: this.subtitulo,
        paragrafos: this.paragrafos.slice(0,3),
        publicacao: this.dataUTF(),
        url: this.url,
        regiao: this.regiao,
        estado: this.estado,
        site: this.site.titulo,
        slug: this.slug
    }
}

exports = module.exports = noticiasSchema;