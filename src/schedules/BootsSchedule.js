var mongoose = require("mongoose")
var SiteScheme = require('../models/SiteScheme')
var NoticiasScheme = require('../models/NoticiasScheme')
var Site = mongoose.model('Site', SiteScheme)
var Noticia = mongoose.model('Noticia', NoticiasScheme)
var IndexerBoot = require('../boots/IndexerBoot')
var ReaderBoot = require('../boots/ReaderBoot')
var PublisherBoot = require('../boots/PublisherBoot')

class BootsSchedule {

    static async index() {
        var site = await Site.findOne({'ativo' : 'true'}).sort({lastScraping: 1}).exec()
        if (!site) return
        site.lastScraping = new Date()
        site.save()
        IndexerBoot.run(site, noticias => {
            noticias.forEach(async noticia => {
                try {
                    var n = await Noticia.find({url: noticia.url}).exec()
                    if (n.length === 0) {
                        await noticia.save()
                    }
                } catch (e) {
                    console.log(['error on index', noticia.url, e])
                }
            })
        })
    }

    static async read() {
        var noticias = await Noticia.find()
            .where({'status':'indexed'})
            .limit(10)
            .populate('site')
            .exec()
        if (noticias.length === 0) return
        ReaderBoot.run(noticias, noticias => {
            noticias.forEach(async noticia => {
                try {
                    await noticia.save()
                } catch (e) {
                    console.log(['error on read', noticia.url, e])
                }
            })
        })
    }

    static async publish() {
        var noticias = await Noticia.find()
            .where({'status':'read'})
            .limit(30)
            .populate('site')
            .exec()
        if (noticias.length === 0) return
        PublisherBoot.run(noticias, noticias => {
            noticias.forEach(async noticia => {
                try {
                    await noticia.save()
                } catch (e) {
                    console.log(['error on publish', noticia.url, e])
                }
            })
        })
    }

}

exports = module.exports = BootsSchedule;