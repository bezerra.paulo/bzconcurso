var passport = require('passport');
var Account = require('../models/AccountScheme');

//Controllers
var NoticiasController = require("../controllers/NoticiaController");
var ContatoController = require("../controllers/ContatoController");
var TamaController = require("../controllers/TemaController");
var AccountController = require("../controllers/admin/AccountController");
var AdmNoticiasController = require("../controllers/admin/NoticiaController");
var SiteController = require("../controllers/admin/SiteController");
var SiteIndexController = require("../controllers/admin/SiteIndexController");
var SiteSeletorController = require("../controllers/admin/SiteSeletorController");
var ApiNoticiasController = require("../controllers/api/ApiNoticiasController");

var url = require('url');

function fullUrl(req) {
    return url.format({
        protocol: req.protocol,
        host: req.get('host'),
        pathname: req.originalUrl
    });
}

module.exports = function (app) {

    //Routes
    app.get('/', NoticiasController.index)
    app.get('/noticia/:slug', NoticiasController.get)
    app.get('/imagem/:id', NoticiasController.getImagem)
    app.get('/regiao/:regiao', NoticiasController.regiao)
    app.get('/regiao/:regiao/:estado', NoticiasController.estado)
    app.get('/buscar/:key?', NoticiasController.buscar)
    app.get('/sobre', (req, res) => res.render('sobre.pug', {url: fullUrl(req)}))
    app.get('/contatos', (req, res) => res.render('contatos.pug', {url: fullUrl(req)}))
    app.post('/contatos', ContatoController.enviar)
    app.get('/temas', TamaController.buscar)
    app.get('/partialListaNoticias', NoticiasController.getPartialListaNoticias)
    app.get('/partialBuscaNoticias', NoticiasController.getPartialBuscaNoticias)

    //Admin Routes
    app.get('/admin', (req, res) => res.redirect('/admin/home'))
    app.get('/admin/login', AccountController.login)
    app.post('/admin/login', passport.authenticate('local', {
        successRedirect: '/admin/home',
        failureRedirect: '/admin/login',
        failureFlash: 'Nome de usuário ou senha incorretos.'
    }))
    app.get('/admin/registerAdmin', AccountController.registerAdmin)
    app.get('/admin/logout', isLoggedIn, AccountController.logout)
    app.get('/admin/home', isLoggedIn, AccountController.home)


    //Cadastro de usuários
    app.get('/admin/accounts', isLoggedIn, isAdmin, AccountController.index)
    app.route('/admin/accounts/new')
        .get(isLoggedIn, isAdmin, AccountController.new)
        .post(isLoggedIn, isAdmin, AccountController.create)
    app.route('/admin/accounts/edit/:userId')
        .get(isLoggedIn, isAdmin, AccountController.edit)
        .post(isLoggedIn, isAdmin, AccountController.update)
    app.get('/admin/accounts/remove/:userId', isLoggedIn, isAdmin, AccountController.remove)
    app.route('/admin/accounts/change_password/:userId')
        .get(isLoggedIn, isAdmin, AccountController.changePassword)
        .post(isLoggedIn, isAdmin, AccountController.savePassword)

    //Cadastro de notícias
    app.get('/admin/noticias', isLoggedIn, AdmNoticiasController.index)
    app.get('/admin/noticias/view/:id', isLoggedIn, AdmNoticiasController.view)
    app.get('/admin/noticias/remove/:id', isLoggedIn, AdmNoticiasController.remove)
    app.get('/admin/noticias/remove', isLoggedIn, AdmNoticiasController.removeAll)
    app.get('/admin/noticias/update/:id', isLoggedIn, AdmNoticiasController.update)
    app.get('/admin/noticias/indexarBusca', isLoggedIn, isAdmin, AdmNoticiasController.indexarBusca)
    app.get('/admin/noticias/indexarNoticias', isLoggedIn, isAdmin, AdmNoticiasController.indexarNoticias)


    //Cadastro de usuários
    app.get('/admin/sites', isLoggedIn, isAdmin, SiteController.index)
    app.get('/admin/sites/view/:id', isLoggedIn, isAdmin, SiteController.view)
    app.route('/admin/sites/new')
        .get(isLoggedIn, isAdmin, SiteController.new)
        .post(isLoggedIn, isAdmin, SiteController.create)
    app.route('/admin/sites/edit/:id')
        .get(isLoggedIn, isAdmin, SiteController.edit)
        .post(isLoggedIn, isAdmin, SiteController.update)
    app.get('/admin/sites/remove/:id', isLoggedIn, isAdmin, SiteController.remove)
    app.get('/admin/sites/active/:id', isLoggedIn, isAdmin, SiteController.active)

    app.route('/admin/sites/:id/index/new')
        .get(isLoggedIn, isAdmin, SiteIndexController.new)
        .post(isLoggedIn, isAdmin, SiteIndexController.create)
    app.route('/admin/sites/:id/index/edit/:indexId')
        .get(isLoggedIn, isAdmin, SiteIndexController.edit)
        .post(isLoggedIn, isAdmin, SiteIndexController.update)
    app.get('/admin/sites/:id/index/remove/:indexId', isLoggedIn, isAdmin, SiteIndexController.remove)

    app.route('/admin/sites/:id/seletor/new')
        .get(isLoggedIn, isAdmin, SiteSeletorController.new)
        .post(isLoggedIn, isAdmin, SiteSeletorController.create)
    app.route('/admin/sites/:id/seletor/edit/:seletorId')
        .get(isLoggedIn, isAdmin, SiteSeletorController.edit)
        .post(isLoggedIn, isAdmin, SiteSeletorController.update)
    app.get('/admin/sites/:id/seletor/remove/:seletorId', isLoggedIn, isAdmin, SiteSeletorController.remove)


    app.get('/api/noticias', ApiNoticiasController.index)
    app.get('/api/noticias/:slug', ApiNoticiasController.view)

    function isLoggedIn(req, res, next) {
        if (req.user) {
            app.locals.user = req.user
            return next();
        }
        res.redirect('/admin/login');
    }

    function isAdmin(req, res, next) {
        if (req.user.group === 'ADMIN') {
            return next();
        }
        req.flash('info', 'Apenas usuários com previlégio de administrador podem acessar esta área.')
        res.redirect('/admin/home');
    }
}