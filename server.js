const PORT = process.env.PORT || 3000;

var express = require('express');
var session = require('express-session')
var app = express();
var schedule = require('node-schedule');
var bodyParser = require('body-parser');
var flash = require('connect-flash');
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var MongoStore = require('connect-mongo')(session);
var multer = require('multer');
var upload = new multer();
var moment = require('moment-timezone');
var minify = require('express-minify');

var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/bzc');

app.set('views', './src/views')
app.set('view engine', 'pug')
app.use(minify());
app.use(express.static('public'));
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: true}));
app.use(upload.array());

app.use(session({
    secret: "SGDFGsadfasdfasdj$#@%#$$@#$%@897f30489ruSDF",
    name: "session",
    store: new MongoStore({mongooseConnection: mongoose.connection}),
    proxy: true,
    resave: true,
    saveUninitialized: true
}));
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());
moment.locale('pt-br');
moment.tz.setDefault('America/Campo_Grande');
app.locals.moment = moment;


//Configuração de DB


var Account = require('./src/models/AccountScheme');
passport.use(new LocalStrategy(Account.authenticate()));
passport.serializeUser(Account.serializeUser());
passport.deserializeUser(Account.deserializeUser());

//Agendamentos
var BootsSchedule = require('./src/schedules/BootsSchedule')
var jIndex = schedule.scheduleJob('0 */20 * * * *', BootsSchedule.index);
var jRead = schedule.scheduleJob('*/10 * * * * *', BootsSchedule.read);
var jPublish = schedule.scheduleJob('*/30 * * * * *', BootsSchedule.publish);
BootsSchedule.index()

//Routes
require('./src/configs/routes')(app);

//Escutando porta
app.listen(PORT)

console.log('Magic happens on port ' + PORT);
exports = module.exports = app;

